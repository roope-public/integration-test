const hexToDec = hex => parseInt(hex, 16);

module.exports = {
    /** 
     * Converts the HEX values to an RGB string
     * @param {string} red 0-ff
     * @param {string} green 0-ff
     * @param {string} blue 0-ff
     * @returns {string} rgb value
    */
    hexToRgb: function(red, green, blue){
        const redDec = hexToDec(red);
        const greenDec = hexToDec(green);
        const blueDec = hexToDec(blue);
        rgb = "(" + redDec + "," + greenDec + "," + blueDec + ")";
        return rgb;
    }
}