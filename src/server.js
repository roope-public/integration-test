const express = require('express');
const converter = require('./converter');
const hextorgbconv = require('./hextorgbconv');
const app = express();
const PORT = 3000;

// Welocme endpoint
app.get('/',(req, res) => res.send("Welcome!"));

// RGB to HEX endpoint
app.get('/rgb-to-hex', (req, res) =>{
    const red = Number(req.query.r);
    const green = Number(req.query.g);
    const blue = Number(req.query.b);
    const hex = converter.rgbToHex(red, green,blue);
    res.send(hex);
});

// HEX to RGB endpoint
app.get('/hex-to-rgb', (req, res) =>{
    const red = req.query.r;
    const green = req.query.g;
    const blue = req.query.b;
    const rgb = hextorgbconv.hexToRgb(red, green,blue);
    res.send(rgb);
});

console.log("NODE_ENV: " + process.env.NODE_ENV);

if(process.env.NODE_ENV === 'test'){
    module.exports = app;
} else {
    app.listen(PORT, () => console.log("Server listening..."))
}

