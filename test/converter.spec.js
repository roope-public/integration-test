// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter RGB to HEX", function(){
    context("RGB to Hex conversion", function(){
        it("Converts the basic colors", function(){
            const redHex = converter.rgbToHex(255,0,0);
            expect(redHex).to.equal("#ff0000");

            const blueHex = converter.rgbToHex(0,0,255);
            expect(blueHex).to.equal("#0000ff");
        })

    });
});