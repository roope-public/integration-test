const expect = require("chai").expect;
const request = require("request");
const app = require ("../src/server");
const PORT = 3000;

describe("Color Code Converter API", function(){
    before("Starting server", function(done){
        server = app.listen(PORT, function(){
            done();
        });
    });

    context("REST API Connection", function(){
        const baseurl = `http://localhost:${PORT}`;
        it("returns status 200", function(done){
            const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
            request(url, function(error, response, body){
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
    })

    context("RGB to HEX conversion", function(){
        const baseurl = `http://localhost:${PORT}`;
        it("converts RGB to HEX and returns HEX", function(done){
            const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
            request(url, function(error, response, body){
                expect(body).to.equal("#ff0000");
                done();
            })
        })
    });
    context("HEX to RGB conversion", function(){
        const baseurl = `http://localhost:${PORT}`;
        it("converts HEX to RGB and returns RGB", function(done){
            const url = baseurl + "/hex-to-rgb?r=ff&g=0&b=0";
            request(url, function(error, response, body){
                expect(body).to.equal("(255,0,0)");
                done();
            })
        })
    })
   
    after("Stop server", function(done){
        server.close();
        done();
    });
});