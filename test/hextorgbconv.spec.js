// TDD - unit testing

const expect = require("chai").expect;
const hextorgbconv = require("../src/hextorgbconv");

describe("Color Code Converter HEX to RGB", function(){
    context("HEX to RGB conversion", function(){
        it("Converts the basic colors", function(){
            const redRgb = hextorgbconv.hexToRgb("ff",0,0);
            expect(redRgb).to.equal("(255,0,0)");

            const blueRgb = hextorgbconv.hexToRgb(0,0,"ff");
            expect(blueRgb).to.equal("(0,0,255)");
        })

    });
});