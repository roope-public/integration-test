# 4. Software Testing - Integration testing -Roope Salminen

This project sets up two javascript files (`converter.js` and `hextorgbconv.js`)
that contain functions for converting an RGB value to HEX and vice versa.

The functions are exported as modules and then imported in `server.js`
where they can be used as part of this simple Express application.

The remote repository can be found here.

## Unit tests

Both converter files have their own test files (`converter.spec.js` and `hextorgbconv.spec.js`) that contain a single test to see if the function works as it should. I.e. it converts the color and outputs it in the format specified in the converter functions.

## Integration test

The third test file `server.spec.js` first tests that the HTTP connection works and then tests that the conversion endpoints specified in `server.js` work properly.

Below is a screenshot showing the HTTP response to converting blue HEX to RGB. We can see that the url `http://localhost:3000/hex-to-rgb?r=0&g=0&b=ff` responds with status code 200 and provides the value `(0,0,255)` in the response body. From this we see that both the connection and the conversion work as they should.

![Results](img/integration-test.jpg)